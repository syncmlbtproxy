/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package syncml.bt.proxy.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Enumeration;

/**
 *
 * @author tpasch
 */
public class IOUtil {

	private IOUtil() {
	}

	public static void copy(InputStream in, OutputStream out) throws IOException {
		final byte[] b = new byte[4 * 1024];
		int c;
		do {
			c = in.read(b);
			if (c > 0) {
				out.write(b, 0, c);
			}
			else if (c == 0) {
				Thread.yield();
			}
		} while(c >= 0);
	}

	public static void copy(Reader in, Writer out) throws IOException {
		final char[] b = new char[2 * 1024];
		int c;
		do {
			c = in.read(b);
			if (c > 0) {
				out.write(b, 0, c);
			}
			else if (c == 0) {
				Thread.yield();
			}
		} while(c >= 0);
	}

	public static void copyResource(String res, OutputStream out) throws IOException {
		copy(getResource(res), out);
	}

	public static InputStream getResource(String res) throws IOException {
		final ClassLoader cl = Thread.currentThread().getContextClassLoader();
		final Enumeration<URL> it = cl.getResources(res);
		if (!it.hasMoreElements()) {
			throw new IOException("Resource '" + "' not found");
		}
		final URL url = it.nextElement();
		if (url == null) {
			throw new IOException("Resource '" + "' not found");
		}
		if (it.hasMoreElements()) {
			throw new IOException("Duplicate resource at " + url + " and " + it.nextElement());
		}
		return url.openStream();
	}

}
