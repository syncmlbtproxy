/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package syncml.bt.proxy.util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 *
 * @author tpasch
 */
public class Encoder {

	static final byte[] bmask = { (byte) 0x00, (byte) 0x01, (byte) 0x03, (byte) 0x07,
		(byte) 0x0F, (byte) 0x1F, (byte) 0x3F, (byte) 0x7F, (byte) 0xFF
	};

	static final short[] smask = { (short) 0x0000, (short) 0x0001, (short) 0x0003, (short) 0x0007,
		(short) 0x000F, (short) 0x001F, (short) 0x003F, (short) 0x007F,
		(short) 0x00FF, (short) 0x01FF, (short) 0x03FF, (short) 0x07FF,
		(short) 0x0FFF, (short) 0x1FFF, (short) 0x3FFF, (short) 0x7FFF, (short) 0xFFFF
	};

	static final int[] imask = { 0x00000000, 0x00000001, 0x00000003, 0x00000007,
		0x0000000F, 0x0000001F, 0x0000003F, 0x0000007F,
		0x000000FF, 0x000001FF, 0x000003FF, 0x000007FF,
		0x00000FFF, 0x00001FFF, 0x00003FFF, 0x00007FFF,
		0x0000FFFF, 0x0001FFFF, 0x0003FFFF, 0x0007FFFF,
		0x000FFFFF, 0x001FFFFF, 0x003FFFFF, 0x007FFFFF,
		0x00FFFFFF, 0x01FFFFFF, 0x03FFFFFF, 0x07FFFFFF,
		0x0FFFFFFF, 0x1FFFFFFF, 0x3FFFFFFF, 0x7FFFFFFF,
		0xFFFFFFFF
	};

	static final long[] lmask = { 0x0000000000000000L, 0x0000000000000001L, 0x0000000000000003L, 0x0000000000000007L,
		0x000000000000000FL, 0x000000000000001FL, 0x000000000000003FL, 0x000000000000007FL,
		0x00000000000000FFL, 0x00000000000001FFL, 0x00000000000003FFL, 0x00000000000007FFL,
		0x0000000000000FFFL, 0x0000000000001FFFL, 0x0000000000003FFFL, 0x0000000000007FFFL,
		0x000000000000FFFFL, 0x000000000001FFFFL, 0x000000000003FFFFL, 0x000000000007FFFFL,
		0x00000000000FFFFFL, 0x00000000001FFFFFL, 0x00000000003FFFFFL, 0x00000000007FFFFFL,
		0x0000000000FFFFFFL, 0x0000000001FFFFFFL, 0x0000000003FFFFFFL, 0x0000000007FFFFFFL,
		0x000000000FFFFFFFL, 0x000000001FFFFFFFL, 0x000000003FFFFFFFL, 0x000000007FFFFFFFL,
		0x00000000FFFFFFFFL, 0x00000001FFFFFFFFL, 0x00000003FFFFFFFFL, 0x00000007FFFFFFFFL,
		0x0000000FFFFFFFFFL, 0x0000001FFFFFFFFFL, 0x0000003FFFFFFFFFL, 0x0000007FFFFFFFFFL,
		0x000000FFFFFFFFFFL, 0x000001FFFFFFFFFFL, 0x000003FFFFFFFFFFL, 0x000007FFFFFFFFFFL,
		0x00000FFFFFFFFFFFL, 0x00001FFFFFFFFFFFL, 0x00003FFFFFFFFFFFL, 0x00007FFFFFFFFFFFL,
		0x0000FFFFFFFFFFFFL, 0x0001FFFFFFFFFFFFL, 0x0003FFFFFFFFFFFFL, 0x0007FFFFFFFFFFFFL,
		0x000FFFFFFFFFFFFFL, 0x001FFFFFFFFFFFFFL, 0x003FFFFFFFFFFFFFL, 0x007FFFFFFFFFFFFFL,
		0x00FFFFFFFFFFFFFFL, 0x01FFFFFFFFFFFFFFL, 0x03FFFFFFFFFFFFFFL, 0x07FFFFFFFFFFFFFFL,
		0x0FFFFFFFFFFFFFFFL, 0x1FFFFFFFFFFFFFFFL, 0x3FFFFFFFFFFFFFFFL, 0x7FFFFFFFFFFFFFFFL,
		0xFFFFFFFFFFFFFFFFL
	};

	private final ByteBuffer buffer;

	private final boolean bigEndian;

	private byte bitsTaken = 0;

	public Encoder(ByteBuffer buffer, boolean bigEndian) {
		if (buffer == null) throw new NullPointerException();
		this.buffer = buffer;
		this.bigEndian = bigEndian;
	}

	public void putSigned(byte b) {
		if (bitsTaken != 0) throw new IllegalStateException();
		buffer.put(b);
	}

	public void putSigned(short s) {
		if (bitsTaken != 0) throw new IllegalStateException();
		final byte b1 = (byte) (s & 0x00FF);
		final byte b2 = (byte) (s >> 8);
		if (!bigEndian) {
			buffer.put(b1);
			buffer.put(b2);
		}
		else {
			buffer.put(b2);
			buffer.put(b1);
		}
	}

	public void putSigned(int i) {
		if (bitsTaken != 0) throw new IllegalStateException();
		final byte b1 = (byte) (i & 0x000000FF);
		final byte b2 = (byte) ((i & 0x0000FF00) >> 8);
		final byte b3 = (byte) ((i & 0x00FF0000) >> 16);
		final byte b4 = (byte) (i >> 24);
		if (!bigEndian) {
			buffer.put(b1);
			buffer.put(b2);
			buffer.put(b3);
			buffer.put(b4);
		}
		else {
			buffer.put(b4);
			buffer.put(b3);
			buffer.put(b2);
			buffer.put(b1);
		}
	}

	public void putSigned(long l) {
		if (bitsTaken != 0) throw new IllegalStateException();
		final int i1 = (int) (l & 0x00000000FFFFFFFFL);
		final int i2 = (int) ((l & 0xFFFFFFFF00000000L) >> 32);
		if (!bigEndian) {
			putSigned(i1);
			putSigned(i2);
		}
		else {
			putSigned(i2);
			putSigned(i1);
		}
	}

	public void put(String s, Charset cs) {
		if (bitsTaken != 0) throw new IllegalStateException();
		final byte[] b = s.getBytes(cs);
		buffer.put(b);
	}

	public void append(byte b, byte bits) {
		assert bits > 0 && bits <= 8;
		final int newBitsTaken = bitsTaken + bits;
		final int pos = buffer.position();
		if (newBitsTaken > 8) {
			final short mask = smask[bits];
			final int shift = 16 - newBitsTaken;
			final byte old;
			if (bitsTaken > 0) {
				old = buffer.get();
				buffer.position(pos);
			}
			else {
				old = 0;
			}
			final short s = (short) (((b & mask) << shift) | (old << 8));
			bitsTaken = 0;
			putSigned(s);
		}
		else {
			final byte mask = bmask[bits];
			final int shift = 8 - newBitsTaken;
			final byte old;
			if (bitsTaken > 0) {
				old = buffer.get();
				buffer.position(pos);
			}
			else {
				old = 0;
			}
			final byte bb = (byte) (((b & mask) << shift) | old);
			bitsTaken = 0;
			putSigned(bb);
			bitsTaken = (byte) newBitsTaken;
		}
		if (newBitsTaken > 0) {
			if (newBitsTaken >= 8) {
				bitsTaken = (byte) (newBitsTaken - 8);
				buffer.position(pos + 1);
			}
			else {
				buffer.position(pos);
			}
		}
		assert bitsTaken >= 0 && bitsTaken < 8;
	}
	
	public void append(short s, byte bits) {
		assert bits > 0 && bits <= 16;
		if (bits <= 8) {
			append((byte) s, bits);
			return;
		}
		final int newBitsTaken = bitsTaken + bits;
		final int pos = buffer.position();
		if (newBitsTaken > 16) {
			final int mask = imask[bits];
			final int shift = 24 - newBitsTaken;
			final byte old;
			if (bitsTaken > 0) {
				old = buffer.get();
				buffer.position(pos);
			}
			else {
				old = 0;
			}
			final int i = (int) (((s & mask) << shift) | (old << 16));
			bitsTaken = 0;
			putSigned(i << 8);
		}
		else {
			final short mask = smask[bits];
			final int shift = 16 - newBitsTaken;
			final byte old;
			if (bitsTaken > 0) {
				old = buffer.get();
				buffer.position(pos);
			}
			else {
				old = 0;
			}
			final short ss = (short) (((s & mask) << shift) | (old << 8));
			bitsTaken = 0;
			putSigned(ss);
			bitsTaken = (byte) newBitsTaken;
		}
		if (newBitsTaken > 0) {
			if (newBitsTaken >= 16) {
				bitsTaken = (byte) (newBitsTaken - 16);
				buffer.position(pos + 2);
			}
			else if (newBitsTaken >= 8) {
				bitsTaken = (byte) (newBitsTaken - 8);
				buffer.position(pos + 1);
			}
			else {
				// buffer.position(pos);
				throw new IllegalStateException();
			}
		}
		assert bitsTaken >= 0 && bitsTaken < 8 : "bitsTaken: " + bitsTaken;
	}

	public void append(int i, byte bits) {
		assert bits > 0 && bits <= 32;
		if (bits <= 8) {
			append((byte) i, bits);
			return;
		}
		else if (bits <= 16) {
			append((short) i, bits);
			return;
		}
		final int newBitsTaken = bitsTaken + bits;
		final int pos = buffer.position();
		if (newBitsTaken > 24) {
			final long mask = lmask[bits];
			final int shift = 32 - newBitsTaken;
			final byte old;
			if (bitsTaken > 0) {
				old = buffer.get();
				buffer.position(pos);
			}
			else {
				old = 0;
			}
			final long l = (long) (((i & mask) << shift) | (old << 24));
			bitsTaken = 0;
			putSigned(l << 32);
		}
		else {
			final int mask = imask[bits];
			final int shift = 24 - newBitsTaken;
			final byte old;
			if (bitsTaken > 0) {
				old = buffer.get();
				buffer.position(pos);
			}
			else {
				old = 0;
			}
			final int ii = (int) (((i & mask) << shift) | (old << 16));
			bitsTaken = 0;
			putSigned(ii << 8);
			bitsTaken = (byte) newBitsTaken;
		}
		if (newBitsTaken > 0) {
			if (newBitsTaken >= 24) {
				bitsTaken = (byte) (newBitsTaken - 24);
				buffer.position(pos + 3);
			}
			else if (newBitsTaken >= 16) {
				bitsTaken = (byte) (newBitsTaken - 16);
				buffer.position(pos + 2);
			}
			else {
				// buffer.position(pos);
				throw new IllegalStateException();
			}
		}
		assert bitsTaken >= 0 && bitsTaken < 8 : "bitsTaken: " + bitsTaken;
	}

	public boolean equalsTo(byte[] buffer, byte bitsTaken) {
		final ByteBuffer b = ByteBuffer.wrap(buffer);
		b.position(b.limit());
		return equalsTo(b, bitsTaken);
	}
	
	public boolean equalsTo(ByteBuffer buffer, byte bitsTaken) {
		final int limit1 = this.buffer.limit();
		final int limit2 = buffer.limit();
		final int pos1 = this.buffer.position();
		final int pos2 = buffer.position();
		if (this.bitsTaken > 0) {
			this.buffer.position(pos1 + 1);
		}
		this.buffer.flip();
		buffer.flip();
		boolean result = this.bitsTaken == bitsTaken && this.buffer.equals(buffer);
		this.buffer.position(pos1);
		buffer.position(pos2);
		this.buffer.limit(limit1);
		buffer.limit(limit2);
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Encoder)) return false;
		final Encoder other = (Encoder) o;
		return bitsTaken == other.bitsTaken && buffer.equals(other.buffer);
	}
	
	@Override
	public int hashCode() {
		int result = bitsTaken;
		result += 3 * buffer.hashCode() + 5;
		return result;
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		result.append("Encoder: ");
		final int pos = buffer.position();
		buffer.rewind();
		while (buffer.position() <= pos) {
			result.append(toString(buffer.get()));
			result.append(" ");
		}
		result.append("bitsTaken: ");
		result.append(bitsTaken);
		buffer.position(pos);
		return result.toString();
	}

	private String toString(byte b) {
		final int i;
		if (b < 0) {
			// treat b as unsigned
			i = b + 0x100;
		}
		else {
			i = b;
		}
		String result = Integer.toHexString(i);
		final int len = result.length();
		if (len > 2) {
			result = result.substring(0, 2);
		}
		else if (len == 1) {
			result = "0" + result;
		}
		return result;
	}

}
