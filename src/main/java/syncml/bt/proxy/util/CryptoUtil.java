/* Erstellt am 13.11.2009 durch thomas */
package syncml.bt.proxy.util;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;

/**
 * CryptoUtil.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class CryptoUtil {
	
	private CryptoUtil() {
	}
	
	public static byte[] md5(ByteBuffer input) throws GeneralSecurityException {
		final MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(input);
		return md.digest();
	}

}
//end of class
