/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt;

import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;

import syncml.bt.proxy.ICommand;

/**
 * SearchCommand.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class SearchCommand implements ICommand<SearchResponse> {
	
	private final int[] attrSet;
	
	private final UUID[] uuidSet;
	
	private final RemoteDevice btDev;
	
	public SearchCommand(int[] attrSet, UUID[] uuidSet, RemoteDevice btDev) {
		this.attrSet = attrSet;
		this.uuidSet = uuidSet;
		this.btDev = btDev;
	}

	public int[] getAttrSet() {
		return attrSet;
	}

	public UUID[] getUuidSet() {
		return uuidSet;
	}

	public RemoteDevice getBtDev() {
		return btDev;
	}

}
//end of class