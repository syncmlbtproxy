/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;

import syncml.bt.proxy.IAsyncCallback;
import syncml.bt.proxy.ICommand;
import syncml.bt.proxy.IService;
import syncml.bt.proxy.bt.raw.InquiryListener;

/**
 * InquiryService.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class InquiryService implements IService<InquiryResponse> {
	
	private final LocalDevice ld;
	
	private final DeviceClass dc;
	
	public InquiryService() throws BluetoothStateException {
		ld = LocalDevice.getLocalDevice();
		final String localAddr = ld.getBluetoothAddress();
		final String localName = ld.getFriendlyName();
		int discover = -1;
		try {
			discover = ld.getDiscoverable();
		}
		catch (RuntimeException e) {
			// bluecove-gpl 2.1.0 bug? (only sometime)
			// do nothing
		}
		System.out.println("local: " + localName + " (" + localAddr + ") " + discover);
		//
		dc = ld.getDeviceClass();
		System.out.println("maj:" + dc.getMajorDeviceClass() + " min:" + dc.getMinorDeviceClass() + " serv:" + dc.getServiceClasses());
	}
	
	@Override
	public InquiryResponse execute(ICommand<InquiryResponse> command) throws BluetoothStateException, InterruptedException {
		final DiscoveryAgent da = ld.getDiscoveryAgent();
		final InquiryListener dl = new InquiryListener(da, this);
		synchronized (this) {
			if (!da.startInquiry(DiscoveryAgent.GIAC, dl)) {
				throw new IllegalStateException("failed to start inquiry");
			}
			wait();
			if (!da.startInquiry(DiscoveryAgent.LIAC, dl)) {
				throw new IllegalStateException("failed to start inquiry");
			}
			wait();
		}
		System.out.println(dl.getRemoteDeviceMap());
		return new InquiryResponse(dl.getRemoteDeviceMap());
	}

	@Override
	public void execute(ICommand<InquiryResponse> command, IAsyncCallback<InquiryResponse> callback) {
		final DiscoveryAgent da = ld.getDiscoveryAgent();
		final InquiryListener dl = new InquiryListener(da, callback);
		try {
			if (!da.startInquiry(DiscoveryAgent.LIAC, dl)) {
				throw new IllegalStateException("failed to start inquiry");
			}
		}
		catch (Exception e) {
			callback.onFailure(e);
		}
	}

}
//end of class
