/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt;

import java.util.Map;

import javax.bluetooth.RemoteDevice;

import syncml.bt.proxy.IResponse;

/**
 * InquiryResponse.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class InquiryResponse implements IResponse {
	
	private final Map<String,RemoteDevice> addr2device;
	
	public InquiryResponse(Map<String,RemoteDevice> map) {
		if (map == null) throw new NullPointerException();
		this.addr2device = map;
	}

	public Map<String,RemoteDevice> getRemoteDeviceMap() {
		return addr2device;
	}

}
//end of class