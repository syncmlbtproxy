/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;

import syncml.bt.proxy.IAsyncCallback;
import syncml.bt.proxy.ICommand;
import syncml.bt.proxy.IService;
import syncml.bt.proxy.bt.raw.InquiryListener;
import syncml.bt.proxy.bt.raw.SearchListener;

/**
 * SearchService.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class SearchService implements IService<SearchResponse> {

	private final LocalDevice ld;

	private final DeviceClass dc;

	public SearchService() throws BluetoothStateException {
		ld = LocalDevice.getLocalDevice();
		final String localAddr = ld.getBluetoothAddress();
		final String localName = ld.getFriendlyName();
		int discover = -1;
		try {
			discover = ld.getDiscoverable();
		}
		catch (RuntimeException e) {
			// ignore
			e.printStackTrace();
		}
		System.out.println("local: " + localName + " (" + localAddr + ") " + discover);
		//
		dc = ld.getDeviceClass();
		System.out.println("maj:" + dc.getMajorDeviceClass() + " min:" + dc.getMinorDeviceClass() + " serv:" + dc.getServiceClasses());
	}

	@Override
	public SearchResponse execute(ICommand<SearchResponse> command) throws Exception {
		final DiscoveryAgent da = ld.getDiscoveryAgent();
		final SearchListener sl = new SearchListener(da, this);
		synchronized (this) {
			final SearchCommand sc = (SearchCommand) command;
			final int transId = da.searchServices(sc.getAttrSet(), sc.getUuidSet(), sc.getBtDev(), sl);
			wait();
		}
		// System.out.println(sl.getServiceRecords());
		return new SearchResponse(sl.getServiceRecords());
	}

	@Override
	public void execute(ICommand<SearchResponse> command, IAsyncCallback<SearchResponse> callback) {
		final DiscoveryAgent da = ld.getDiscoveryAgent();
		final InquiryListener sl = new InquiryListener(da, callback);
		try {
			if (command instanceof SearchCommand) {
				final SearchCommand sc = (SearchCommand) command;
				final int transId = da.searchServices(sc.getAttrSet(), sc.getUuidSet(), sc.getBtDev(), sl);
			}
		}
		catch (Exception e) {
			callback.onFailure(e);
		}
	}

}
//end of class