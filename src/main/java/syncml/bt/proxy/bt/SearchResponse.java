/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt;

import java.util.Set;

import javax.bluetooth.ServiceRecord;

import syncml.bt.proxy.IResponse;

/**
 * SearchResponse.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class SearchResponse implements IResponse {
	
	private final Set<ServiceRecord> records;
	
	public SearchResponse(Set<ServiceRecord> records) {
		if (records == null) throw new NullPointerException();
		this.records = records;
	}
	
	public Set<ServiceRecord> getServiceRecords() {
		return records;
	}

}
//end of class