/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt.raw;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;

import syncml.bt.proxy.IAsyncCallback;
import syncml.bt.proxy.bt.InquiryResponse;

/**
 * SyncmlDiscoveryListener.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class InquiryListener implements DiscoveryListener {

	private static final int INQUIRY_TOO_EARLY = -1;
	
	private final DiscoveryAgent da;

	private final Object notify;
	
	private final IAsyncCallback<InquiryResponse> callback;

	private final Map<String, RemoteDevice> addr2device = new ConcurrentHashMap<String, RemoteDevice>();

	private volatile int completed = INQUIRY_TOO_EARLY;

	public InquiryListener(DiscoveryAgent da, Object notify) {
		if (da == null || notify == null)
			throw new NullPointerException();
		this.da = da;
		this.notify = notify;
		if (notify instanceof IAsyncCallback<?>) {
			this.callback = (IAsyncCallback<InquiryResponse>) notify;
		}
		else {
			this.callback = null;
		}
	}

	@Override
	public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
		try {
			System.out.println("Found " + btDevice.getFriendlyName(false) + " (" + btDevice.getBluetoothAddress() + ") with class " + cod);
			addr2device.put(btDevice.getBluetoothAddress(), btDevice);
		}
		catch (IOException e) {
			if (callback != null) {
				da.cancelInquiry(this);
				callback.onFailure(e);
			}
			else {
				throw new IllegalStateException(e.toString());
			}
		}
	}

	@Override
	public void inquiryCompleted(int discType) {
		System.out.println("inquiryCompleted " + discType + " (" + toString(discType) + ")");
		completed = discType;
		// handle sync and async calls different
		if (callback != null) {
			callback.onSuccess(new InquiryResponse(getRemoteDeviceMap()));
		}
		else {
			synchronized (notify) {
				notify.notify();
			}
		}
	}

	@Override
	public void serviceSearchCompleted(int transID, int respCode) {
		System.out.println("serviceSearchCompleted " + transID + " " + respCode);
		throw new IllegalStateException();
	}

	@Override
	public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
		System.out.println("servicesDiscovered " + transID + " " + Arrays.asList(servRecord));
		throw new IllegalStateException();
	}

	public Map<String, RemoteDevice> getRemoteDeviceMap() {
		if (completed == INQUIRY_TOO_EARLY)
			throw new IllegalStateException("Too early");
		return addr2device;
	}

	private static String toString(int inquiry) {
		switch (inquiry) {
		case INQUIRY_COMPLETED:
			return "INQUIRY_COMPLETED";
		case INQUIRY_TERMINATED:
			return "INQUIRY_TERMINATED";
		case INQUIRY_ERROR:
			return "INQUIRY_ERROR";
		default:
			throw new IllegalArgumentException("Unknown: " + inquiry);
		}
	}

}
//end of class
