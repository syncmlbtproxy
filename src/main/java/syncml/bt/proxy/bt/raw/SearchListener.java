/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy.bt.raw;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;

import syncml.bt.proxy.IAsyncCallback;
import syncml.bt.proxy.bt.SearchResponse;

/**
 * SearchListener.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class SearchListener implements DiscoveryListener {

	private static final int SEARCH_TOO_EARLY = -1;
	
	private final DiscoveryAgent da;

	private final Object notify;
	
	private final IAsyncCallback<SearchResponse> callback;

	private final Set<ServiceRecord> records = new HashSet<ServiceRecord>();

	private volatile int completed = SEARCH_TOO_EARLY;

	public SearchListener(DiscoveryAgent da, Object notify) {
		if (da == null || notify == null)
			throw new NullPointerException();
		this.da = da;
		this.notify = notify;
		if (notify instanceof IAsyncCallback<?>) {
			this.callback = (IAsyncCallback<SearchResponse>) notify;
		}
		else {
			this.callback = null;
		}
	}

	@Override
	public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
		throw new IllegalStateException();
	}

	@Override
	public void inquiryCompleted(int discType) {
		throw new IllegalStateException();
	}

	@Override
	public void serviceSearchCompleted(int transID, int respCode) {
		System.out.println("serviceSearchCompleted trans="  + transID + " " + respCode + " (" + toString(respCode) + ")");
		completed = respCode;
		// handle sync and async calls different
		if (callback != null) {
			callback.onSuccess(new SearchResponse(getServiceRecords()));
		}
		else {
			synchronized (notify) {
				notify.notify();
			}
		}
	}

	@Override
	public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
		System.out.println("servicesDiscovered trans="  + transID + " " + Arrays.asList(servRecord));
		for (ServiceRecord r: servRecord) {
			records.add(r);
		}
	}

	public Set<ServiceRecord> getServiceRecords() {
		if (completed == SEARCH_TOO_EARLY) throw new IllegalStateException("Too early");
		return records;
	}

	private static String toString(int search) {
		switch (search) {
		case SERVICE_SEARCH_COMPLETED:
			return "SERVICE_SEARCH_COMPLETED";
		case SERVICE_SEARCH_DEVICE_NOT_REACHABLE:
			return "SERVICE_SEARCH_DEVICE_NOT_REACHABLE";
		case SERVICE_SEARCH_ERROR:
			return "SERVICE_SEARCH_ERROR";
		case SERVICE_SEARCH_NO_RECORDS:
			return "SERVICE_SEARCH_NO_RECORDS";
		case SERVICE_SEARCH_TERMINATED:
			return "SERVICE_SEARCH_TERMINATED";
		default:
			throw new IllegalArgumentException("Unknown: " + search);
		}
	}

}
//end of class