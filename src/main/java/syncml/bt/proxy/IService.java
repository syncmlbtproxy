/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy;

/**
 * IService.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public interface IService<T extends IResponse> {
	
	// sync call - may block for a long time
	T execute(ICommand<T> command) throws Exception;
	
	// async call - returns immediately, but uses a callback
	void execute(ICommand<T> command, IAsyncCallback<T> callback);	

}
//end of class