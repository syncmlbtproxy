/* Erstellt am 13.11.2009 durch thomas */
package syncml.bt.proxy.syncml.ds.alertedsync;

/**
 * Notification.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class Notification {
	
	private NotificationHdr hdr;
	
	private NotificationBody body;
	
	public Notification() {
	}

	public NotificationHdr getHdr() {
		return hdr;
	}

	public void setHdr(NotificationHdr hdr) {
		this.hdr = hdr;
	}

	public NotificationBody getBody() {
		return body;
	}

	public void setBody(NotificationBody body) {
		this.body = body;
	}

}
//end of class
