/* Erstellt am 13.11.2009 durch thomas */
package syncml.bt.proxy.syncml.ds.alertedsync;

/**
 * Sync.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class Sync {
	
	// 4-bit
	private byte syncType;
	
	// 4-bit
	private byte future;

	// 3 byte (24-bit)
	private int contentType;
	
	// serverUri length (1 byte)
	// private byte serverUriLen;
	
	// serverUri
	private String serverUri;
	
	// n*BIT
	private byte[] vendorSpecific;

	public Sync() {
	}

	public byte getSyncType() {
		return syncType;
	}

	public void setSyncType(byte syncType) {
		this.syncType = syncType;
	}

	public byte getFuture() {
		return future;
	}

	public void setFuture(byte future) {
		this.future = future;
	}

	public int getContentType() {
		return contentType;
	}

	public void setContentType(int contentType) {
		this.contentType = contentType;
	}

	public String getServerUri() {
		return serverUri;
	}

	public void setServerUri(String serverUri) {
		this.serverUri = serverUri;
	}

	public byte[] getVendorSpecific() {
		return vendorSpecific;
	}

	public void setVendorSpecific(byte[] vendorSpecific) {
		this.vendorSpecific = vendorSpecific;
	}
	
}
//end of class
