/* Erstellt am 13.11.2009 durch thomas */
package syncml.bt.proxy.syncml.ds.alertedsync;

/**
 * NotificationHdr.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class NotificationHdr {
	
	// 10-bit
	private short version;
	
	// 2-bit
	private byte uiMode;
	
	// 1-bit
	private byte initiator;
	
	// 27-bit (rest of 5 bytes)
	private byte future;
	
	// 2-bytes
	private short sessionId;
	
	// server name length: 1 byte
	// private byte serverNameLength;
	
	private String server;
	
	public NotificationHdr() {
	}

	public short getVersion() {
		return version;
	}

	public void setVersion(short version) {
		this.version = version;
	}

	public byte getUiMode() {
		return uiMode;
	}

	public void setUiMode(byte uiMode) {
		this.uiMode = uiMode;
	}

	public byte getInitiator() {
		return initiator;
	}

	public void setInitiator(byte initiator) {
		this.initiator = initiator;
	}

	public byte getFuture() {
		return future;
	}

	public void setFuture(byte future) {
		this.future = future;
	}

	public short getSessionId() {
		return sessionId;
	}

	public void setSessionId(short sessionId) {
		this.sessionId = sessionId;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

}
//end of class
