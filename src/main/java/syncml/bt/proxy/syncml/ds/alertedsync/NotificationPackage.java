/* Erstellt am 13.11.2009 durch thomas */
package syncml.bt.proxy.syncml.ds.alertedsync;

/**
 * NotificationPackage.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class NotificationPackage {
	
	// 128 bit
	private byte[] digest;
	
	private Notification notification;
	
	public NotificationPackage() {
	}

	public byte[] getDigest() {
		return digest;
	}

	public void setDigest(byte[] digest) {
		this.digest = digest;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

}
//end of class
