/* Erstellt am 13.11.2009 durch thomas */
package syncml.bt.proxy.syncml.ds.alertedsync;

/**
 * NotificationBody.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class NotificationBody {
	
	// 4-bit
	private byte numberOfSyncs;
	
	// 4-bit
	private byte future;
	
	private Sync sync;
		
	public NotificationBody() {
	}

	public byte getNumberOfSyncs() {
		return numberOfSyncs;
	}

	public void setNumberOfSyncs(byte numberOfSyncs) {
		this.numberOfSyncs = numberOfSyncs;
	}

	public byte getFuture() {
		return future;
	}

	public void setFuture(byte future) {
		this.future = future;
	}

	public Sync getSync() {
		return sync;
	}

	public void setSync(Sync sync) {
		this.sync = sync;
	}
	
}
//end of class
