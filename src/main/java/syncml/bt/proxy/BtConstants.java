/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy;

import javax.bluetooth.UUID;

/**
 * BtConstants.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class BtConstants {
	
   public static final UUID OBEX_OBJECT_PUSH = new UUID(0x1105);
   public static final UUID OBEX_FILE_TRANSFER = new UUID(0x1106);
   public static final int OBEX_CONNECTION_ID = 0xCB;
   
   public static final int NAME_ATTR = 0x0100;
   
   /*
   Base UUID Value (Used in promoting 16-bit and 32-bit UUIDs to 128-bit UUIDs)	0x0000000000001000800000805F9B34FB 	128-bit
   SDP	0x0001	16-bit
   RFCOMM	0x0003	16-bit
   OBEX	0x0008	16-bit
   HTTP	0x000C	16-bit
   L2CAP	0x0100	16-bit
   BNEP	0x000F	16-bit
   Serial Port	0x1101	16-bit
   ServiceDiscoveryServerServiceClassID	0x1000 	16-bit
   BrowseGroupDescriptorServiceClassID	0x1001 	16-bit
   PublicBrowseGroup	0x1002	16-bit
   OBEX Object Push Profile	0x1105	16-bit
   OBEX File Transfer Profile	0x1106	16-bit
   Personal Area Networking User	0x1115 	16-bit
   Network Access Point	0x1116	16-bit
   Group Network	0x1117	16-bit
   */
   
   // public static final UUID BASE_VALUE = new UUID("0x0000000000001000800000805F9B34FB", false);
   
   public static final UUID SDP = new UUID(0x0001);
   public static final UUID RFCOMM = new UUID(0x0003);
   public static final UUID OBEX = new UUID(0x0008);
   public static final UUID HTTP = new UUID(0x000C);
   public static final UUID L2CAP = new UUID(0x0100);
   public static final UUID BNEP = new UUID(0x000F);
   public static final UUID SERIAL = new UUID(0x1101);
   public static final UUID PAN_USER = new UUID(0x1115);
   public static final UUID NAP = new UUID(0x1116);
   public static final UUID GROUP_NETWORK = new UUID(0x1117);
   
   public static final UUID SYNCML_SERVER = new UUID("000000010000100080000002EE000002", false);
   public static final UUID SYNCML_CLIENT= new  UUID("000000020000100080000002EE000002", false);
   // for nokia copy, see http://www.traud.de/gsm/nokiaSeries40-5th.htm
   public static final UUID NOKIA_SYNCML_SERVER = new UUID("000056010000100080000002EE000001", false);
   // for nokia car kits, see http://www.traud.de/gsm/nokiaSeries40.htm
   public static final UUID NOKIA_CARKIT_SERVER = new UUID("000050010000100080000002EE000001", false);
   // public static final UUID SYNCML_SERVER = new UUID("00000001-0000-1000-8000-0002EE000002", false);
   public static final String SYNCML_SERVER_NAME = "SyncMLServer";
   public static final String SYNCML_CLIENT_NAME = "SyncMLClient";
   public static final byte[] TARGET_SYNCML_SYNC = "SYNCML-SYNC".getBytes();
   public static final byte[] TARGET_SYNCML_DM = "SYNCML-DM".getBytes();
   public static final String TYPE_SYNCML_SYNC_XML = "application/vnd.syncml+xml";
   public static final String TYPE_SYNCML_SYNC_WBXML = "application/vnd.syncml+wbxml";
   public static final String TYPE_SYNCML_DM_XML = "application/vnd.syncml.dm+xml";
   public static final String TYPE_SYNCML_DM_WBXML = "application/vnd.syncml.dm+wbxml";
   public static final String TYPE_CONTACT = "text/x-vcard";
   public static final String TYPE_EVENT = "text/x-vcalendar";
   public static final String TYPE_NOTE = "text/plain";
   public static final String TYPE_SMS = "text/x-vMessage";
   //
   // http://www.traud.de/gsm/SyncML.htm
   // Server Alerted Notification (SyncML 1.2)
   public static final String TYPE_SYNCML_DM_NOTIFICATION = "application/vnd.syncml.notification";
   public static final String TYPE_SYNCML_DS_NOTIFICATION = "application/vnd.syncml.ds.notification";
   
}
//end of class
