/* Erstellt am 15.10.2009 durch thomas */
package syncml.bt.proxy;

/**
 * AsyncCallback.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public interface IAsyncCallback<T extends IResponse> {
	
	void onFailure(Throwable oops);
	
	void onSuccess(T result);

}
//end of class