import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.Arrays;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connection;
import javax.microedition.io.Connector;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;
import javax.obex.ServerRequestHandler;
import javax.obex.SessionNotifier;
import junit.framework.TestCase;
import syncml.bt.proxy.BtConstants;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tpasch
 */
public class SimpleServerTest extends TestCase {

	private static class RequestHandler extends ServerRequestHandler {

		private final PrintStream out;

		public RequestHandler(PrintStream out) {
			this.out = out;
		}

		@Override
		public int onPut(Operation op) {
			out.println("connection ID=" + getConnectionID());
			System.out.println("onPut " + op);
			try {
				HeaderSet hs = op.getReceivedHeaders();
				String name = ( String)hs.getHeader(HeaderSet.NAME);
				if (name != null) {
					System.out.println("put name:" + name);
				}

				InputStream is = op.openInputStream();

				StringBuffer buf = new StringBuffer();
				int data;
				while ((data = is.read()) != -1) {
					buf.append(( char)data);
				}

				System.out.println("got:" + buf.toString());

				op.close();
				return ResponseCodes.OBEX_HTTP_OK;
			}
			catch (IOException e) {
				e.printStackTrace();
				return ResponseCodes.OBEX_HTTP_UNAVAILABLE;
			}
		}

		@Override
		public int onGet(Operation op) {
			out.println("connection ID=" + getConnectionID());
			out.println("onPut " + op);
			return ResponseCodes.OBEX_HTTP_OK;
		}

		@Override
		public int onConnect(HeaderSet request, HeaderSet reply) {
			try {
				// Fill in Connection ID
				if (getConnectionID() < 0) {
					// setConnectionID(92982);
					// reply.setHeader(BtConstants.OBEX_CONNECTION_ID, 92982);
				}
				// Fill in who according to spec
				final byte[] target = ( byte[])request.getHeader(HeaderSet.TARGET);
				out.println("connection ID=" + getConnectionID() + " target=" + new String(target));
				if (target == null || target.length < 1) {
					throw new IllegalStateException();
				}
				reply.setHeader(HeaderSet.WHO, target);
				// Fill in Name
				reply.setHeader(HeaderSet.NAME, "PC Suite");
			}
			catch (IOException e) {
				e.printStackTrace();
				throw new IllegalStateException();
			}
			out.println("onConnect " + toString(request) + "\nreply " + toString(reply));
			return ResponseCodes.OBEX_HTTP_OK;
		}

		@Override
		public void onDisconnect(HeaderSet request, HeaderSet reply) {
			out.println("connection ID=" + getConnectionID());
			out.println("onDisconnect " + toString(request) + "\nreply " + toString(reply));
		}
		
		@Override
		public int onSetPath(HeaderSet request, HeaderSet reply, boolean backup, boolean create) {
			out.println("connection ID=" + getConnectionID());
			out.println("onSetPath " + toString(request) + "\nreply " + toString(reply));
			return ResponseCodes.OBEX_HTTP_OK;
		}
		
		@Override
		public int onDelete(HeaderSet request, HeaderSet reply) {
			out.println("connection ID=" + getConnectionID());
			out.println("onDelete " + toString(request) + "\nreply " + toString(reply));
			return ResponseCodes.OBEX_HTTP_OK;
		}
		
		@Override
		public void onAuthenticationFailure(byte[] userName) {
			out.println("connection ID=" + getConnectionID());
			out.println("onAuthenticationFailure " + new String(userName));
		}

		public String toString(HeaderSet set) {
			final StringBuilder result = new StringBuilder();
			try {
				result.append("HeaderSet result=");
				// result.append(set.getResponseCode());
				result.append('\n');
				final int[] headers = set.getHeaderList();
				if (headers != null) {
					for (int h : headers) {
						result.append(h);
						result.append('=');
						final Object o = set.getHeader(h);
						if (o instanceof byte[]) {
							result.append(new String(( byte[])o));
						}
						else {
							result.append(o);
						}
						if (o != null) {
							result.append(" of ");
							result.append(o.getClass());
						}
						result.append('\n');
					}
				}
				return result.toString();
			}
			catch (IOException e) {
				e.printStackTrace();
				return result.toString();
			}
		}
	}

	public SimpleServerTest() {}

	public void testServerSync() throws Exception {
		// LocalDevice.getLocalDevice().setDiscoverable(DiscoveryAgent.GIAC);
		final String addr = LocalDevice.getLocalDevice().getBluetoothAddress();
		final String url = "btgoep://localhost:" + BtConstants.SYNCML_CLIENT + ";name=" + BtConstants.SYNCML_CLIENT_NAME;
		// final String url = "btgoep://localhost:" + BtConstants.NOKIA_SYNCML_SERVER + ";name=" + BtConstants.SYNCML_CLIENT_NAME;
		// final String url = "btgoep://localhost:" + BtConstants.SYNCML_SERVER + ";name=" + BtConstants.SYNCML_SERVER_NAME;
		// final String url = "btgoep://localhost:" + BtConstants.SYNCML_SERVER + ";name=" + "PC Suite";
		System.out.println("url=" + url);
		final SessionNotifier serverConnection = ( SessionNotifier)Connector.open(url);

		int count = 0;
		while (count < 1) {
			RequestHandler handler = new RequestHandler(System.out);
			Connection con = serverConnection.acceptAndOpen(handler);
			System.out.println("Received OBEX connection " + (++count) + " of " + con.getClass());
			// describe(con.getClass());
			Thread.sleep(10000);
		}

	}
	
	private static void describe(Class<?> clazz) {
		final Method[] methods = clazz.getDeclaredMethods();
		for (Method m: methods) {
			System.out.println(m.getReturnType() + " " + m.getName() + "(" + Arrays.asList(m.getParameterTypes()) + ")");
		}
	}
}
