import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

import javax.microedition.io.Connector;
import javax.obex.ClientSession;
import javax.obex.HeaderSet;
import javax.obex.Operation;
import javax.obex.ResponseCodes;
import syncml.bt.proxy.BtConstants;
import syncml.bt.proxy.IAsyncCallback;
import syncml.bt.proxy.bt.InquiryResponse;
import syncml.bt.proxy.bt.InquiryService;
import syncml.bt.proxy.bt.SearchCommand;
import syncml.bt.proxy.bt.SearchResponse;
import syncml.bt.proxy.bt.SearchService;
import syncml.bt.proxy.bt.raw.InquiryListener;

import junit.framework.TestCase;
import syncml.bt.proxy.util.IOUtil;

/* Erstellt am 15.10.2009 durch thomas */

/**
 * SimpleTest.java ist ...
 * @author thomas
 * @version $Id: Java_CodeStyle_CodeTemplates.xml,v 1.1 2005/05/31 09:20:45 stefan.schrader Exp $
 * TODO thomas javadoc für Klasse
 */
public class SimpleTest extends TestCase {

	public void tstRaw() throws Exception {
		final LocalDevice ld = LocalDevice.getLocalDevice();
		final String localAddr = ld.getBluetoothAddress();
		final String localName = ld.getFriendlyName();
		final int discover = ld.getDiscoverable();
		System.out.println("local: " + localName + " (" + localAddr + ") " + discover);
		//
		final DeviceClass dc = ld.getDeviceClass();
		System.out.println("maj:" + dc.getMajorDeviceClass() + " min:" + dc.getMinorDeviceClass() + " serv:" + dc.getServiceClasses());
		//
		final DiscoveryAgent da = ld.getDiscoveryAgent();
		final InquiryListener dl = new InquiryListener(da, this);
		synchronized (this) {
			if (!da.startInquiry(DiscoveryAgent.GIAC, dl)) {
				throw new IllegalStateException("failed to start inquiry");
			}
			wait();
			if (!da.startInquiry(DiscoveryAgent.LIAC, dl)) {
				throw new IllegalStateException("failed to start inquiry");
			}
			wait();
		}
		System.out.println(dl.getRemoteDeviceMap());
	}

	public void tstNote() throws Exception {
		final InquiryService is = new InquiryService();
		final InquiryResponse ir = is.execute(null);
		System.out.println(ir.getRemoteDeviceMap());
		final Set<String> urls = new HashSet<String>();
		//
		if (ir.getRemoteDeviceMap().size() > 0) {
			final SearchService ss = new SearchService();
			final int[] attrSet = new int[] { BtConstants.NAME_ATTR };
			// final UUID[] uuidSet = new UUID[] { BtConstants.OBEX_FILE_TRANSFER, BtConstants.OBEX_OBJECT_PUSH, BtConstants.OBEX, BtConstants.RFCOMM };
			final UUID[] uuidSet = new UUID[] { BtConstants.SYNCML_CLIENT };
			for (String addr : ir.getRemoteDeviceMap().keySet()) {
				final RemoteDevice btDev = ir.getRemoteDeviceMap().get(addr);
				final SearchCommand sc = new SearchCommand(attrSet, uuidSet, btDev);
				System.out.println("Searching on " + btDev.getFriendlyName(false) + " (" + btDev.getBluetoothAddress() + ")");
				//
				final SearchResponse sr = ss.execute(sc);
				final Set<ServiceRecord> records = sr.getServiceRecords();
				System.out.println(records);
				for (ServiceRecord r: records) {
					final String url = r.getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
					if (url != null) {
						urls.add(url);
						System.out.println("device=" + r.getHostDevice().getBluetoothAddress() + " (" + r.getHostDevice().getFriendlyName(false) + ") url=" + url);
					}
				}
			}
		}
		for (String url : urls) {
			System.out.println("Connect to " + url);
			final ClientSession clientSession = (ClientSession) Connector.open(url);
			try {
				System.out.println("connected by " + clientSession);
				final HeaderSet request = clientSession.createHeaderSet();
				request.setHeader(HeaderSet.TARGET, BtConstants.TARGET_SYNCML_SYNC);
				final HeaderSet reply = clientSession.connect(request);
				System.out.println("reply is " + reply);
				if (reply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
					System.out.println("Failed to connect with response code " + reply.getResponseCode());
					continue;
				}

				final HeaderSet hsOperation = clientSession.createHeaderSet();
				hsOperation.setHeader(HeaderSet.NAME, "Hello.txt");
				hsOperation.setHeader(HeaderSet.TYPE, BtConstants.TYPE_NOTE);

				//Create PUT Operation
				final Operation putOperation = clientSession.put(hsOperation);
				OutputStream os = null;
				try {
					// Send some text to server
					byte data[] = "Hello world!".getBytes("iso-8859-1");
					os = putOperation.openOutputStream();
					os.write(data);
				} finally {
					if (os != null) os.close();
					putOperation.close();
				}
			} finally {
				try {
					clientSession.disconnect(null);
					clientSession.close();
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void tstAsync() throws Exception {
		final InquiryService is = new InquiryService();
		is.execute(null, new IAsyncCallback<InquiryResponse>() {
			@Override
			public void onSuccess(InquiryResponse result) {
				System.out.println(result);
			}

			@Override
			public void onFailure(Throwable oops) {
				oops.printStackTrace();
			}
		});
		System.out.println("Sleep");
		Thread.sleep(30000);
		System.out.println("end testAsync");
	}

	public void testNotification() throws Exception {
		final String mime = BtConstants.TYPE_SYNCML_DS_NOTIFICATION;
		System.out.println("Connect to " + TestConstants.MY_URL);
		final ClientSession clientSession = (ClientSession) Connector.open(TestConstants.MY_URL);
		try {
			System.out.println("connected by " + clientSession);
			final HeaderSet request = clientSession.createHeaderSet();
			request.setHeader(HeaderSet.TARGET, BtConstants.TARGET_SYNCML_SYNC);
			final HeaderSet reply = clientSession.connect(request);
			System.out.println("reply is " + reply);
			if (reply.getResponseCode() != ResponseCodes.OBEX_HTTP_OK) {
				System.out.println("Failed to connect with response code " + reply.getResponseCode());
			}

			final HeaderSet hsOperation = clientSession.createHeaderSet();
			hsOperation.setHeader(HeaderSet.NAME, "PC Suite");
			hsOperation.setHeader(HeaderSet.TYPE, BtConstants.TYPE_SYNCML_DS_NOTIFICATION);

			//Create PUT Operation
			final Operation putOperation = clientSession.put(hsOperation);
			OutputStream os = null;
			try {
				os = putOperation.openOutputStream();
				IOUtil.copyResource("nokiaAlert.xml", os);
			} finally {
				if (os != null) {
					os.close();
				}
				putOperation.close();
			}
		} finally {
			Thread.sleep(10000);
			try {
				clientSession.disconnect(null);
				clientSession.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
//end of class
