
import java.nio.ByteBuffer;

import junit.framework.TestCase;
import syncml.bt.proxy.util.Encoder;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tpasch
 */
public class BitTest extends TestCase {
	
	public BitTest() {
	}

	public void testSignedNumbers() {
		final byte b = (byte) 0x7f;
		final short s = b;
		System.out.println("b " + Integer.toHexString(b));
		System.out.println("s " + Integer.toHexString(s));
	}
	
	public void testEncoderByte() {
		System.out.println("testEncoderByte");
		final ByteBuffer buffer = ByteBuffer.allocate(1024);
		final Encoder enc = new Encoder(buffer, true);
		enc.putSigned((byte) 0xff);
		System.out.println(enc.toString());
		enc.putSigned((byte) 0x0f);
		System.out.println(enc.toString());
		enc.append((byte) 2, (byte) 3);
		// result: 1111 1111 0000 1111 010[0 0000] bitsTaken: 3
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xff, (byte) 0x0f, (byte) 0x40 }, (byte) 3));
		enc.append((byte) 2, (byte) 3);
		// result: 1111 1111 0000 1111 0100 10[00] bitsTaken: 6
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xff, (byte) 0x0f, (byte) 0x48 }, (byte) 6));
		enc.append((byte) 2, (byte) 3);
		// result: 1111 1111 0000 1111 0100 1001 0[000 0000] bitsTaken: 1
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xff, (byte) 0x0f, (byte) 0x49, (byte) 0x0 }, (byte) 1));
	}

	public void testEncoderShort() {
		System.out.println("testEncoderShort");
		final ByteBuffer buffer = ByteBuffer.allocate(1024);
		final Encoder enc = new Encoder(buffer, true);
		enc.putSigned((byte) 0xf0);
		// result: 0f
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0}, (byte) 0));
		enc.append((short) 0x0785, (byte) 12);
		// result: 0f 78 5 bitsTaken: 4
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0, (byte) 0x78, (byte) 0x50}, (byte) 4));
		enc.append((short) 0x0785, (byte) 11);
		// result: 0f 78 5f 0a bitsTaken: 7
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0, (byte) 0x78, (byte) 0x5f, (byte) 0x0a}, (byte) 7));
		enc.append((short) 0xe785, (byte) 16);
		// result: 0f 78 5f 0b cf 0a bitsTaken: 7
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0, (byte) 0x78, (byte) 0x5f, (byte) 0x0b, (byte) 0xcf, (byte) 0x0a}, (byte) 7));
	}

	public void testEncoderInt() {
		System.out.println("testEncoderInt");
		final ByteBuffer buffer = ByteBuffer.allocate(1024);
		final Encoder enc = new Encoder(buffer, true);
		enc.putSigned((byte) 0xf0);
		// result: 0f
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0}, (byte) 0));
		enc.append(0x07854ae1, (byte) 28);
		// result: 0f 78 54 ae 10 bitsTaken: 4
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0, (byte) 0x78, (byte) 0x54, (byte) 0xae, (byte) 0x10}, (byte) 4));
		enc.append(0x1a785, (byte) 17);
		// result: 0f 78 54 ae 1d 3c 28 bitsTaken: 5
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0, (byte) 0x78, (byte) 0x54, (byte) 0xae, (byte) 0x1d, (byte) 0x3c, (byte) 0x28}, (byte) 5));
		enc.append(0x7e785, (byte) 19);
		// result: 0f 78 54 ae 1d 3c 2f e7 85 bitsTaken: 7
		System.out.println(enc.toString());
		assertTrue(enc.equalsTo(new byte[] { (byte) 0xf0, (byte) 0x78, (byte) 0x54, (byte) 0xae, (byte) 0x1d, (byte) 0x3c, (byte) 0x2f, (byte) 0xe7, (byte) 0x85}, (byte) 0));
	}

}
